// while loop

function whileLoop(){
	let count = 5;

	while(count != 0){
		console.log('The current count is '+ count);
		count--;
	}
}

// Do-While Loop

function doWhileLoop(){
	let count = 20;

	do {
		console.log(count);
		count--;
	} while (count > 0);
}

// for loop
function forLoop(){
	for(let count = 0; count <= 20; count++){
		console.log(count);
	}
}

// continue and break
function modifiedForLoop(){
	for(let count = 0; count <= 20; count++){
		if (count % 2 === 0){
			continue;
		}
		console.log(count);
		if(count  > 10 ){
			break;
		}
	}
}

// continue - allows the code to go to the next iteration of the loop without finishing the remainder of the code block
// break - ends the execution o fthe current loop


function multiplication(x){
	for(let x =1; x<=10; x++){
		console.log(x);
		
		for (let y = 1; y <= 10; y++){
			
			let total = x*y;
			console.log( x +" * "+y+" = "+total);
		
		}	
	}
}	
